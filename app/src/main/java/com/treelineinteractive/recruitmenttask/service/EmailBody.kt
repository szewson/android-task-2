package com.treelineinteractive.recruitmenttask.service

import javax.mail.Address
import javax.mail.Authenticator
import javax.mail.internet.InternetAddress

data class EmailData(
    val addressesTo: List<InternetAddress>,
    val addressesCC: List<InternetAddress> = emptyList(),
    val addressesBCC: List<InternetAddress> = emptyList(),
    val addressFrom: InternetAddress,
    val emailSubject: String,
    val emailBody: String
)
