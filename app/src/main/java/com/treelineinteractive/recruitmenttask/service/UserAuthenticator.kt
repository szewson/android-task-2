package com.treelineinteractive.recruitmenttask.service

import javax.mail.Authenticator
import javax.mail.PasswordAuthentication

class UserAuthenticator(
    private val username: String,
    private val password: String
) : Authenticator() {
    override fun getPasswordAuthentication(): PasswordAuthentication? {
        return PasswordAuthentication(username, password)
    }
}