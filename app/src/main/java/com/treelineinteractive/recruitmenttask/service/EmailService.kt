package com.treelineinteractive.recruitmenttask.service

import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import java.util.*

object EmailService {

    fun sendEmail(server: String, port: String, email: EmailData) {
        val auth = UserAuthenticator("username", "password")
        val props = Properties()
        props["mail.smtp.auth"] = "true"
        props["mail.user"] = email.addressFrom
        props["mail.smtp.host"] = server
        props["mail.smtp.port"] = port
        props["mail.smtp.starttls.enable"] = "true"
        props["mail.smtp.ssl.trust"] = server
        props["mail.mime.charset"] = "UTF-8"
        val msg: Message = MimeMessage(Session.getDefaultInstance(props, auth))
        msg.setFrom(email.addressFrom)
        msg.sentDate = Calendar.getInstance().time
        msg.setRecipients(Message.RecipientType.TO, email.addressesTo.toTypedArray())
        msg.setRecipients(Message.RecipientType.CC, email.addressesCC.toTypedArray())
        msg.setRecipients(Message.RecipientType.BCC, email.addressesBCC.toTypedArray())
        msg.replyTo = arrayOf(email.addressFrom)

        msg.addHeader("X-Mailer", "ANDROID_CLIENT")
        msg.addHeader("Precedence", "bulk")
        msg.subject = email.emailSubject

        msg.setContent(MimeMultipart().apply {
            addBodyPart(MimeBodyPart().apply {
                setText(email.emailBody, "iso-8859-1")
            })
        })
        Transport.send(msg)
    }
}