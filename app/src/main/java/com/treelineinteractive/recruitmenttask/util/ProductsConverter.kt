package com.treelineinteractive.recruitmenttask.util

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.ui.model.ProductItemViewData
import java.lang.StringBuilder

object ProductsConverter {
    fun List<ProductItem>.toProductItemViewDataList(): List<ProductItemViewData> {
        val products = ArrayList<ProductItemViewData>()
        for (item in this) {
            products.add(
                ProductItemViewData(
                    id = item.id,
                    title = item.title,
                    description = item.description,
                    available = item.available,
                    sold = 0
                )
            )
        }
        return products
    }

    fun List<ProductItemViewData>.toReportEmail(): String {
        val reportBuilder = StringBuilder()
        for(product in this) {
            reportBuilder.append("Product: ${product.title} \n")
            reportBuilder.append("Sold: ${product.sold} \n \n")
        }
        return reportBuilder.toString()
    }
}