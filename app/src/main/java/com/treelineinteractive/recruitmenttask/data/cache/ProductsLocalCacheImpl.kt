package com.treelineinteractive.recruitmenttask.data.cache

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.JsonSyntaxException
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import javax.inject.Inject
import kotlin.coroutines.coroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.isActive

class ProductsLocalCacheImpl internal constructor(
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson,
) : ProductsLocalCache {

    private val productItemListType = object : TypeToken<ArrayList<ProductItem?>?>() {}.type

    @Inject
    constructor(context: Context, gson: Gson) : this(
        context.getSharedPreferences(PRODUCTS_STORE, Context.MODE_PRIVATE),
        gson
    )

    override suspend fun saveProducts(products: List<ProductItem>) {
        sharedPreferences.edit().putString(CACHED_PRODUCTS_KEY, gson.toJson(products)).apply()
    }

    override fun getProducts(): Flow<List<ProductItem>> = flow {
        while (coroutineContext.isActive) {
            val products = try {
                gson.fromJson<List<ProductItem>>(
                    sharedPreferences.getString(CACHED_PRODUCTS_KEY, ""),
                    productItemListType
                )
            } catch (e: JsonSyntaxException) {
                emptyList()
            }
            emit(products)
            delay(PRODUCTS_FLOW_DELAY)
        }
    }

    private companion object {
        const val PRODUCTS_STORE = "products_store"
        const val CACHED_PRODUCTS_KEY = "cached_products_key"
        const val PRODUCTS_FLOW_DELAY = 1000L
    }
}