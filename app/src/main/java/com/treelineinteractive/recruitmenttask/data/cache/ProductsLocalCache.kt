package com.treelineinteractive.recruitmenttask.data.cache

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import kotlinx.coroutines.flow.Flow

interface ProductsLocalCache {

    suspend fun saveProducts(products: List<ProductItem>)

    fun getProducts(): Flow<List<ProductItem>>
}