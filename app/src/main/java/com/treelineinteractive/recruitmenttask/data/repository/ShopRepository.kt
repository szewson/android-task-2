package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import kotlinx.coroutines.flow.Flow

interface ShopRepository {
    /**
     * Returns a flow of all products from cache combined with fetched data from API
     *
     * @return flow of [RepositoryResult] of list of [ProductItem]
     */
    fun getProducts(): Flow<RepositoryResult<List<ProductItem>>>
}