package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.cache.ProductsLocalCache
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.supervisorScope

@ExperimentalCoroutinesApi
class ShopRepositoryImpl @Inject constructor(
    private val shopService: ShopService,
    private val productsLocalCache: ProductsLocalCache
) : ShopRepository {

    override fun getProducts(): Flow<RepositoryResult<List<ProductItem>>> {
        return combine(
            fetchProducts().flowOn(Dispatchers.IO),
            productsLocalCache.getProducts().distinctUntilChanged()
        ) { status, data ->
            RepositoryResult(data, status)
        }
    }

    private fun fetchProducts(): Flow<RepositoryRequestStatus> = channelFlow {
        send(RepositoryRequestStatus.FETCHING)

        supervisorScope {
            try {
                productsLocalCache.saveProducts(shopService.getInventory())
                send(RepositoryRequestStatus.COMPLETE)
            } catch (e: Exception) {
                e.printStackTrace()
                send(RepositoryRequestStatus.Error(e))
            }
        }
    }
}