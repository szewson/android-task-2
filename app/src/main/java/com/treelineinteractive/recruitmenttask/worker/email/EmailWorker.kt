package com.treelineinteractive.recruitmenttask.worker.email

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.GsonBuilder
import com.treelineinteractive.recruitmenttask.service.EmailData
import com.treelineinteractive.recruitmenttask.service.EmailService
import com.treelineinteractive.recruitmenttask.worker.EmailSenderImpl.Companion.EMAIL_DATA_KEY
import javax.mail.MessagingException


class EmailWorker(
    context: Context,
    parameters: WorkerParameters
) : CoroutineWorker(context, parameters) {

    override suspend fun doWork(): Result {
        val gson = GsonBuilder().create()
        Log.d("DODO", "Email data: ${inputData.getString(EMAIL_DATA_KEY)}")
        val email = gson.fromJson(inputData.getString(EMAIL_DATA_KEY), EmailData::class.java)
        return try {
            EmailService.sendEmail("my_server", "my_port", email)
            Result.success()
        } catch (e: MessagingException) {
            Result.failure()
        }
    }
}