package com.treelineinteractive.recruitmenttask.worker

import com.treelineinteractive.recruitmenttask.service.EmailData

interface EmailSender {

    suspend fun sendEmail(email: EmailData)
}