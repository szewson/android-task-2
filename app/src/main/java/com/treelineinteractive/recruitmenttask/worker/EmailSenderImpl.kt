package com.treelineinteractive.recruitmenttask.worker

import androidx.work.BackoffPolicy
import androidx.work.Constraints
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.google.gson.Gson
import com.treelineinteractive.recruitmenttask.service.EmailData
import com.treelineinteractive.recruitmenttask.worker.email.EmailWorker
import javax.inject.Inject
import java.util.concurrent.TimeUnit

class EmailSenderImpl @Inject constructor(
    private val workManager: WorkManager,
    private val gson: Gson
) : EmailSender {

    override suspend fun sendEmail(email: EmailData) {
        val emailRequest = prepareEmail(email)

        workManager.enqueueUniqueWork(
            EMAIL_WORK_NAME,
            ExistingWorkPolicy.KEEP,
            emailRequest
        )
    }

    private fun prepareEmail(email: EmailData) = OneTimeWorkRequestBuilder<EmailWorker>()
        .addTag(EMAIL_WORK_NAME)
        .setInputData(workDataOf(EMAIL_DATA_KEY to gson.toJson(email)))
        .setConstraints(getNetworkConstraints())
        .setBackoffCriteria(
            BackoffPolicy.LINEAR,
            BACKOFF_TIME,
            TimeUnit.SECONDS
        )
        .build()

    private fun getNetworkConstraints() = Constraints.Builder()
        .setRequiredNetworkType(NetworkType.CONNECTED)
        .build()

    companion object {
        const val EMAIL_DATA_KEY = "email_data"
        private const val EMAIL_WORK_NAME = "email_work"
        private const val BACKOFF_TIME = 30L
    }
}