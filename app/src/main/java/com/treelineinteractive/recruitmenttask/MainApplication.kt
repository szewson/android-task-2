package com.treelineinteractive.recruitmenttask

import android.app.Application
import android.content.Context
import com.treelineinteractive.recruitmenttask.di.ApiModule
import com.treelineinteractive.recruitmenttask.di.EmailSenderModule
import com.treelineinteractive.recruitmenttask.di.MainActivityModule
import com.treelineinteractive.recruitmenttask.di.ProductsLocalCacheModule
import com.treelineinteractive.recruitmenttask.di.ShopRepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject
import javax.inject.Singleton

class MainApplication : Application(), HasAndroidInjector {

    override fun onCreate() {
        super.onCreate()
        initDagger()
    }

    private lateinit var mainApplicationComponent: MainApplicationComponent

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    override fun androidInjector() = androidInjector

    private fun initDagger() {
        mainApplicationComponent = DaggerMainApplicationComponent.builder()
            .app(this)
            .build()

        mainApplicationComponent.inject(this)
    }
}

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApiModule::class,
        ShopRepositoryModule::class,
        ProductsLocalCacheModule::class,
        EmailSenderModule::class,
        MainActivityModule::class
    ]
)
interface MainApplicationComponent {
    fun inject(app: MainApplication)

    @Component.Builder
    interface Builder {
        fun build(): MainApplicationComponent

        @BindsInstance
        fun app(app: Context): Builder
    }
}