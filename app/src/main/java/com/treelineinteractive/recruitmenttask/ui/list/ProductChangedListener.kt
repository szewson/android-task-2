package com.treelineinteractive.recruitmenttask.ui.list

interface ProductChangedListener {
    fun onProductChange(productId: String, soldChange: Int)
}