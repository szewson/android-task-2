package com.treelineinteractive.recruitmenttask.ui.list

import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding
import com.treelineinteractive.recruitmenttask.ui.model.ProductItemViewData

class ProductViewHolder(
    private val itemBinding: ViewProductItemBinding
) : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(item: ProductItemViewData, listener: ProductChangedListener) {
        itemBinding.descriptionLabel.text = item.description
        itemBinding.nameLabel.text = item.title
        itemBinding.availableText.text = item.available.toString()
        itemBinding.soldText.text = item.sold.toString()

        itemBinding.upButton.setOnClickListener {
            listener.onProductChange(productId = item.id, soldChange = 1)
        }

        itemBinding.downButton.setOnClickListener {
            listener.onProductChange(productId = item.id, soldChange = -1)
        }
    }
}