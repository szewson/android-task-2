package com.treelineinteractive.recruitmenttask.ui

import com.treelineinteractive.recruitmenttask.data.repository.RepositoryRequestStatus
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.service.EmailData
import com.treelineinteractive.recruitmenttask.service.UserAuthenticator
import com.treelineinteractive.recruitmenttask.ui.model.ProductItemViewData
import com.treelineinteractive.recruitmenttask.ui.viewmodel.BaseAction
import com.treelineinteractive.recruitmenttask.ui.viewmodel.BaseViewModel
import com.treelineinteractive.recruitmenttask.ui.viewmodel.BaseViewState
import com.treelineinteractive.recruitmenttask.util.ProductsConverter.toProductItemViewDataList
import com.treelineinteractive.recruitmenttask.worker.EmailSender
import javax.inject.Inject
import javax.mail.internet.InternetAddress
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import androidx.lifecycle.viewModelScope
import com.treelineinteractive.recruitmenttask.util.ProductsConverter.toReportEmail

class MainViewModel @Inject internal constructor(
    private val shopRepository: ShopRepository,
    private val emailSender: EmailSender
) : BaseViewModel<MainViewModel.MainViewState, MainViewModel.MainViewAction>(MainViewState()) {

    private var products = ArrayList<ProductItemViewData>()

    data class MainViewState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val items: List<ProductItemViewData> = listOf(),
        val emailSent: Boolean = false
    ) : BaseViewState {
        val isSuccess: Boolean
            get() = !isLoading && error == null
    }

    sealed class MainViewAction : BaseAction {
        object LoadingProducts : MainViewAction()
        data class ProductsLoaded(val items: List<ProductItemViewData>) : MainViewAction()
        data class ProductsLoadingError(val error: String) : MainViewAction()
        object EmailSent : MainViewAction()
        data class EmailSentError(val error: String) : MainViewAction()
    }

    fun sendEmailToBoss() {
        viewModelScope.launch {
            kotlin.runCatching {
                emailSender.sendEmail(
                    EmailData(
                        addressesTo = listOf(InternetAddress("bossman@bosscompany.com")),
                        addressFrom = InternetAddress("worker1@bosscompany.com"),
                        emailSubject = "Sales Report",
                        emailBody = products.toReportEmail()
                    )
                )
            }
                .onSuccess { sendAction(MainViewAction.EmailSent) }
                .onFailure { sendAction(MainViewAction.EmailSentError("Error while trying to send the email")) }

        }
    }

    fun loadProducts() {
        viewModelScope.launch {
            shopRepository.getProducts()
                .collect { result ->
                    when (result.requestStatus) {
                        is RepositoryRequestStatus.FETCHING -> {
                            sendAction(MainViewAction.LoadingProducts)
                        }
                        is RepositoryRequestStatus.COMPLETE -> {
                            if (!result.data.isNullOrEmpty()) {
                                products.clear()
                                products.addAll(result.data.toProductItemViewDataList())
                                sendAction(MainViewAction.ProductsLoaded(products))
                            }
                        }
                        is RepositoryRequestStatus.Error -> {
                            sendAction(MainViewAction.ProductsLoadingError("Oops, something went wrong"))
                        }
                    }
                }
        }
    }

    fun updateProductSoldValue(productId: String, soldChange: Int) {
        val productIndex = products.indexOfFirst {
            it.id == productId
        }
        val updatedProduct = products[productIndex].copy()
        if (updatedProduct.sold + soldChange <= updatedProduct.available && updatedProduct.sold + soldChange >= 0) {
            updatedProduct.sold += soldChange
        }
        products[productIndex] = updatedProduct
        sendAction(MainViewAction.ProductsLoaded(products))
    }

    override fun onReduceState(viewAction: MainViewAction): MainViewState = when (viewAction) {
        is MainViewAction.LoadingProducts -> state.copy(isLoading = true, error = null)
        is MainViewAction.ProductsLoaded -> state.copy(
            isLoading = false,
            error = null,
            items = viewAction.items
        )
        is MainViewAction.ProductsLoadingError -> state.copy(
            isLoading = false,
            error = viewAction.error
        )
        is MainViewAction.EmailSent -> state.copy(
            isLoading = false,
            error = null,
            emailSent = true
        )
        is MainViewAction.EmailSentError -> state.copy(
            error = viewAction.error
        )
    }
}