package com.treelineinteractive.recruitmenttask.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.ui.MainViewModel
import com.treelineinteractive.recruitmenttask.worker.EmailSender
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory @Inject constructor(
    private val shopRepository: ShopRepository,
    private val emailSender: EmailSender
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(shopRepository, emailSender) as T
    }
}