package com.treelineinteractive.recruitmenttask.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding
import com.treelineinteractive.recruitmenttask.ui.model.ProductItemViewData

class ProductListAdapter(
    private val onProductChangedListener: ProductChangedListener
) : RecyclerView.Adapter<ProductViewHolder>() {

    private val products = ArrayList<ProductItemViewData>()

    fun updateProducts(newProducts: List<ProductItemViewData>) {
        val productDiffCallback = ProductDiffCallback(this.products, newProducts)
        val diffResult = DiffUtil.calculateDiff(productDiffCallback)

        this.products.clear()
        this.products.addAll(newProducts)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val itemBinding =
            ViewProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = products[position]
        holder.bind(product, onProductChangedListener)
    }

    override fun getItemCount(): Int = products.size

    override fun getItemViewType(position: Int): Int {
        return position
    }
}