package com.treelineinteractive.recruitmenttask.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.ui.list.ProductChangedListener
import com.treelineinteractive.recruitmenttask.ui.list.ProductListAdapter
import com.treelineinteractive.recruitmenttask.ui.viewmodel.MainViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(), ProductChangedListener {

    @Inject
    lateinit var viewModelFactory: MainViewModelFactory
    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(
            MainViewModel::class.java
        )
    }

    private lateinit var adapter: ProductListAdapter
    private val binding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupAdapter()

        mainViewModel.loadProducts()

        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }

        binding.sendEmailButton.setOnClickListener {
            mainViewModel.sendEmailToBoss()
        }

        mainViewModel.stateLiveData.observe { state ->
            binding.progressBar.isVisible = state.isLoading
            binding.errorLayout.isVisible = state.error != null
            binding.errorLabel.text = state.error

            binding.productList.isVisible = state.isSuccess

            if (state.emailSent) {
                Toast.makeText(this, "Email sent successfully!", Toast.LENGTH_LONG).show()
            }

            if (state.isSuccess) {
                adapter.updateProducts(state.items.toList())
                binding.sendEmailButton.visibility = View.VISIBLE
            }
        }
    }

    private fun setupAdapter() {
        adapter = ProductListAdapter(this).also {
            binding.productList.layoutManager = LinearLayoutManager(this)
            binding.productList.adapter = it
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }

    override fun onProductChange(productId: String, soldChange: Int) {
        mainViewModel.updateProductSoldValue(productId, soldChange)
    }
}