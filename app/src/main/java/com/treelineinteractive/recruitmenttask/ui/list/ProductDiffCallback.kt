package com.treelineinteractive.recruitmenttask.ui.list

import androidx.recyclerview.widget.DiffUtil
import com.treelineinteractive.recruitmenttask.ui.model.ProductItemViewData

class ProductDiffCallback(
    private val oldProducts: List<ProductItemViewData>,
    private val newProducts: List<ProductItemViewData>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldProducts.size

    override fun getNewListSize(): Int = newProducts.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldProducts[oldItemPosition].id == newProducts[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldProduct = oldProducts[oldItemPosition]
        val newProduct = newProducts[newItemPosition]
        return oldProduct.sold == newProduct.sold
    }
}