package com.treelineinteractive.recruitmenttask.ui.model

data class ProductItemViewData(
    var id: String,
    var title: String,
    var description: String,
    var available: Int,
    var sold: Int
) {
    override fun equals(other: Any?): Boolean {
        if (other?.javaClass != javaClass) return false
        return this.id == (other as ProductItemViewData).id
    }

    override fun hashCode(): Int {
        return this.id.hashCode()
    }
}
