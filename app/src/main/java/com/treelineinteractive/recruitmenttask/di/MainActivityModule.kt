package com.treelineinteractive.recruitmenttask.di

import com.treelineinteractive.recruitmenttask.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityModule {
    @ContributesAndroidInjector
    fun mainActivity(): MainActivity
}