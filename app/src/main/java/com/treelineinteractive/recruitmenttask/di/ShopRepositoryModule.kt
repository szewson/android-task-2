package com.treelineinteractive.recruitmenttask.di

import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface ShopRepositoryModule {
    @Binds
    fun bindShopRepositoryModule(impl: ShopRepositoryImpl): ShopRepository
}