package com.treelineinteractive.recruitmenttask.di

import com.treelineinteractive.recruitmenttask.data.cache.ProductsLocalCache
import com.treelineinteractive.recruitmenttask.data.cache.ProductsLocalCacheImpl
import dagger.Binds
import dagger.Module

@Module
interface ProductsLocalCacheModule {
    @Binds
    fun bindProductsLocalCacheModule(impl: ProductsLocalCacheImpl): ProductsLocalCache
}