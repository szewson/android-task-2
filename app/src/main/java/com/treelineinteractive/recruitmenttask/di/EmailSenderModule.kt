package com.treelineinteractive.recruitmenttask.di

import android.content.Context
import androidx.work.WorkManager
import com.google.gson.Gson
import com.treelineinteractive.recruitmenttask.worker.EmailSender
import com.treelineinteractive.recruitmenttask.worker.EmailSenderImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
class EmailSenderModule {

    @Provides
    fun providesEmailSender(applicationContext: Context, gson: Gson): EmailSender {
        return EmailSenderImpl(WorkManager.getInstance(applicationContext), gson)
    }
}