object Plugins {
    const val KOTLIN_ANDROID = "kotlin-android"
    const val ANDROID_APPLICATION = "com.android.application"
    const val KOTLIN_KAPT = "kotlin-kapt"
}